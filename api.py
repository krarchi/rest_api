import json
from flask import Flask

app = Flask(__name__)

data = [{'name':'Archi','Age':'24'},{'name':'Rohit','age':'23'},{'name':'Avinash','age':'20'}]
data = json.dumps(data)

@app.route('/run',methods=['GET'])
def run():
    return data

if __name__ == '__main__':
    app.run(debug=True)